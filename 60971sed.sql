-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 09 2020 г., 11:49
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60971sed`
--

-- --------------------------------------------------------

--
-- Структура таблицы `груз`
--

CREATE TABLE `груз` (
  `id` int NOT NULL,
  `id_рейса` int NOT NULL,
  `вес` int NOT NULL,
  `отправитель` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `груз`
--

INSERT INTO `груз` (`id`, `id_рейса`, `вес`, `отправитель`) VALUES
(1, 1, 12000, 'КумАПП'),
(2, 2, 20000, 'ИП Катовский'),
(3, 3, 12000, 'ООО ЗТ'),
(4, 4, 20000, 'ООО Леник'),
(5, 5, 21000, 'Газпромнефть'),
(6, 6, 21000, 'Башнефть'),
(7, 7, 3000, 'Хлебзавод'),
(8, 8, 15000, 'Деловые линии');

-- --------------------------------------------------------

--
-- Структура таблицы `маршрут`
--

CREATE TABLE `маршрут` (
  `id` int NOT NULL,
  `наименование` varchar(255) NOT NULL,
  `тариф` int NOT NULL,
  `расстояние` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `маршрут`
--

INSERT INTO `маршрут` (`id`, `наименование`, `тариф`, `расстояние`) VALUES
(1, 'Кумертау-Уфа', 12, 220),
(2, 'Уфа-Кумертау', 15, 220),
(3, 'Уфа-Сургут', 20, 1600),
(4, 'Сургут-Уфа', 15, 1600),
(5, 'Уфа-Оренбург', 15, 370),
(6, 'Оренбург-Уфа', 12, 370),
(7, 'Сургут-Нижневартовск', 13, 240),
(8, 'Нижневартовск-Сургут', 10, 240);

-- --------------------------------------------------------

--
-- Структура таблицы `рейс`
--

CREATE TABLE `рейс` (
  `id` int NOT NULL,
  `id_маршрута` int NOT NULL,
  `id_транспорта` int NOT NULL,
  `отправление` datetime NOT NULL,
  `прибытие` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `рейс`
--

INSERT INTO `рейс` (`id`, `id_маршрута`, `id_транспорта`, `отправление`, `прибытие`) VALUES
(1, 1, 1, '2020-10-08 11:38:34', '2020-10-08 14:38:34'),
(2, 8, 5, '2020-10-09 11:38:34', '2020-10-09 14:38:34'),
(3, 6, 4, '2020-10-09 11:39:22', '2020-10-09 17:39:22'),
(4, 7, 5, '2020-10-11 11:39:22', '2020-10-11 14:39:22'),
(5, 4, 3, '2020-10-09 11:40:12', '2020-10-11 11:40:12'),
(6, 2, 2, '2020-10-09 11:40:12', '2020-10-09 14:40:12'),
(7, 5, 2, '2020-10-13 11:41:08', '2020-10-13 16:41:08'),
(8, 3, 3, '2020-10-14 11:41:08', '2020-10-16 06:41:08');

-- --------------------------------------------------------

--
-- Структура таблицы `транспорт`
--

CREATE TABLE `транспорт` (
  `id` int NOT NULL,
  `наименование` varchar(255) NOT NULL,
  `грузоподъемность` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `транспорт`
--

INSERT INTO `транспорт` (`id`, `наименование`, `грузоподъемность`) VALUES
(1, 'Volvo FH17', 30000),
(2, 'Volvo FH18', 30000),
(3, 'SCANIA', 30000),
(4, 'MAN', 25000),
(5, 'КАМАЗ 2310', 20000),
(6, 'УРАЛ', 20000),
(7, 'ГАЗ 1253', 3500),
(8, 'ГАЗ 1500', 5000);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `груз`
--
ALTER TABLE `груз`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_рейса` (`id_рейса`);

--
-- Индексы таблицы `маршрут`
--
ALTER TABLE `маршрут`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `рейс`
--
ALTER TABLE `рейс`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_маршрута` (`id_маршрута`),
  ADD KEY `id_транспорта` (`id_транспорта`);

--
-- Индексы таблицы `транспорт`
--
ALTER TABLE `транспорт`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `груз`
--
ALTER TABLE `груз`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `маршрут`
--
ALTER TABLE `маршрут`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `рейс`
--
ALTER TABLE `рейс`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `транспорт`
--
ALTER TABLE `транспорт`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `груз`
--
ALTER TABLE `груз`
  ADD CONSTRAINT `груз_ibfk_1` FOREIGN KEY (`id_рейса`) REFERENCES `рейс` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `рейс`
--
ALTER TABLE `рейс`
  ADD CONSTRAINT `рейс_ibfk_1` FOREIGN KEY (`id_маршрута`) REFERENCES `маршрут` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `рейс_ibfk_2` FOREIGN KEY (`id_транспорта`) REFERENCES `транспорт` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
