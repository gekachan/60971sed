<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Transport extends Migration
{
	public function up()
	{
	    //route
        if (!$this->db->tableexists('route')) {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE),
                'rate' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'distance' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
            ));

            $this->forge->createtable('route', TRUE);
        }

        //transport
        if (!$this->db->tableexists('transport')) {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE),
                'lifting_capacity' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
            ));

            $this->forge->createtable('transport', TRUE);
        }

        //flight
        if (!$this->db->tableexists('flight')) {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_route' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_transport' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'departure' => array('type' => 'DATE', 'null' => FALSE),
                'arrival' => array('type' => 'DATE', 'null' => FALSE),
            ));

            $this->forge->addForeignKey('id_route', 'route', 'id', 'RESTRICT', 'RESRICT');
            $this->forge->addForeignKey('id_transport', 'transport', 'id', 'RESTRICT', 'RESRICT');

            $this->forge->createtable('flight', TRUE);
        }

		//cargo
        if (!$this->db->tableexists('cargo')) {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'sender' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE),
                'weight' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_flight' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));

            $this->forge->addForeignKey('id_flight', 'flight', 'id', 'RESTRICT', 'RESRICT');

            $this->forge->createtable('cargo', TRUE);
        }

	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->droptable('route');
        $this->forge->droptable('transport');
        $this->forge->droptable('cargo');
        $this->forge->droptable('flight');
	}
}
