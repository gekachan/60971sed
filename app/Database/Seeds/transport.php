<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Transport extends Seeder
{
    public function run()
    {
        $data = [
            'name' => 'Кумертау-Уфа',
            'rate' => '12',
            'distance' => '220',
        ];
        $this->db->table('route')->insert($data);
        $data = [
            'name' => 'Уфа-Кумертау',
            'rate' => '12',
            'distance' => '220',
        ];
        $this->db->table('route')->insert($data);
        $data = [
            'name' => 'Уфа-Сургут',
            'rate' => '12',
            'distance' => '1800',
        ];
        $this->db->table('route')->insert($data);
        $data = [
            'name' => 'Сургут-Уфа',
            'rate' => '12',
            'distance' => '1800',
        ];
        $this->db->table('route')->insert($data);
        $data = [
            'name' => 'Сургут-Нефтьюганск',
            'rate' => '12',
            'distance' => '70',
        ];
        $this->db->table('route')->insert($data);
        $data = [
            'name' => 'Нефтьюганск-Сургут',
            'rate' => '12',
            'distance' => '70',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'name' => 'Volvo FH17',
            'lifting_capacity'=>'30000',
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'name' => 'Scania',
            'lifting_capacity'=>'30000',
        ];
        $this->db->table('transport')->insert($data);
        $data = [
            'name' => 'ГАЗ',
            'lifting_capacity'=>'1500',
        ];
        $this->db->table('transport')->insert($data);

        $data = [
            'id_route'=> 1,
            'id_transport'=>1,
            'departure'=>'2020-10-09',
            'arrival'=>'2020-10-09',
        ];
        $this->db->table('flight')->insert($data);

        $data = [
            'id_flight' => 1,
            'weight'=> 20000,
            'sender'=> 'ИП Катовский',
        ];

        $this->db->table('cargo')->insert($data);
    }
}