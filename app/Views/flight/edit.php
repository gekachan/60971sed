<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('flight/update'); ?>
        <input type="hidden" name="id" value="<?= $flight["id"] ?>">

        <div class="form-group">
            <label for="id_route">Маршрут</label>
            <select class="form-control <?= ($validation->hasError('id_route')) ? 'is-invalid' : ''; ?>" name="id_route" id="id_route">
                <?php foreach ($route as $r) { ?>
                    <option <?php if($r['id'] === $flight["id_route"]) { ?> selected="selected" <?php } ?> value="<?= $r['id'] ?>"><?= $r['name'] ?></option>
                <?php } ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_route') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="id_transport">Транспорт</label>
            <select class="form-control <?= ($validation->hasError('id_transport')) ? 'is-invalid' : ''; ?>" name="id_transport" id="id_transport">
                <?php foreach ($transport as $t) { ?>
                    <option <?php if($t['id'] === $flight["id_transport"]) { ?> selected="selected" <?php } ?> value="<?= $t['id'] ?>"><?= $t['name'] ?></option>
                <?php } ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_transport') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="birthday">Дата и время отправления</label>
            <input type="date" class="form-control <?= ($validation->hasError('departure')) ? 'is-invalid' : ''; ?>" name="departure" value="<?= $flight["departure"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('departure') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата и время прибытия</label>
            <input type="date" class="form-control <?= ($validation->hasError('arrival')) ? 'is-invalid' : ''; ?>" name="arrival" value="<?= $flight["arrival"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('arrival') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>