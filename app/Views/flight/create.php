<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?php
    $routename = array();
    foreach ($route as $r){
        $routename[$r['id']] = $r['name'];
    }
    $transportname = array();
    foreach ($transport as $t){
        $transportname[$t['id']] = $t['name'];
    }
?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('flight/store'); ?>

        <div class="form-group">
            <label for="id_route">Маршрут</label>
            <select class="form-control <?= ($validation->hasError('id_route')) ? 'is-invalid' : ''; ?>" name="id_route" id="id_route">
                <?php foreach ($route as $r) { ?>
                    <option value="<?= $r['id'] ?>"><?= $r['name'] ?></option>
                <?php } ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_route') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="id_transport">Транспорт</label>
            <select class="form-control <?= ($validation->hasError('id_transport')) ? 'is-invalid' : ''; ?>" name="id_transport" id="id_transport">
                <?php foreach ($transport as $t) { ?>
                    <option value="<?= $t['id'] ?>"><?= $t['name'] ?></option>
                <?php } ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_transport') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="departure">Дата и время отправления</label>
            <input id="departure" type="date" class="form-control <?= ($validation->hasError('departure')) ? 'is-invalid' : ''; ?>" name="departure">
            <div class="invalid-feedback">
                <?= $validation->getError('departure') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="arrival">Дата и время прибытия</label>
            <input id="arrival" type="date" class="form-control <?= ($validation->hasError('arrival')) ? 'is-invalid' : ''; ?>" name="arrival">
            <div class="invalid-feedback">
                <?= $validation->getError('arrival') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>