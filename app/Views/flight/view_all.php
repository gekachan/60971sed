<?php


use CodeIgniter\I18n\Time; ?>
<?= $this->extend('templates/layout') ?>
<?= $this->section('content')?>
    <div class="container main">
        <h2>Все рейсы</h2>

        <?php if (!empty($flight) && is_array($flight)) : ?>

            <?php
            $routename = array();
            foreach ($route as $r){
                $routename[$r['id']] = $r['name'];
            }
            foreach ($flight as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <?php if ($item['id_transport'] > 0 && $item['id_transport'] < 6) : ?>
                                    <img height="150" src="https://s1.iconbird.com/ico/0612/iconslandtransport/w256h2561339251581TractorUnitBlack.png" class="card-img">
                                <?php else :?>
                                    <img height="150" src="https://pngicon.ru/file/uploads/gruzovik.png" class="card-img">
                                <?php endif ?>
                            <?php else:?>
                                <img src="<?= esc($item['picture_url']); ?>" class="card-img">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($routename[$item['id_route']]); ?></h5>
                                <p class="card-text"><?= esc(Time::parse($item['departure'])->toDateString()); ?></p>
                                <p class="card-text"><?= esc(Time::parse($item['arrival'])->toDateString()); ?></p>
                                <a href="<?= base_url()?>/index.php/flight/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>

                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейсы.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>