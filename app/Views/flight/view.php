<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($flight)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php

                        $routename = array();
                        foreach ($route as $r){
                            $routename[$r['id']] = $r['name'];
                        }
                        $transportname = array();
                        foreach ($transport as $t){
                            $transportname[$t['id']] = $t['name'];
                        }

                        if (is_null($flight['picture_url'])) : ?>
                        <?php if ($flight['id_transport'] > 0 && $flight['id_transport'] < 6) : ?>
                        <img height="150" src="https://s1.iconbird.com/ico/0612/iconslandtransport/w256h2561339251581TractorUnitBlack.png" class="card-img">
                        <?php else :?>
                        <img height="150" src="https://pngicon.ru/file/uploads/gruzovik.png" class="card-img">
                        <?php endif ?>
                        <?php else:?>
                        <img src="<?= esc($flight['picture_url']); ?>" class="card-img">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($routename[$flight['id_route']]); ?></h5>
                            <p class="card-text"><?= esc($transportname[$flight['id_transport']]); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Тип:</div>
                                <?php if ($flight['id_transport'] > 1 && $flight['id_transport'] < 6) : ?>
                                    <div class="text-muted">Тягач</div>
                                <?php else :?>
                                    <div class="text-muted">Грузовик</div>
                                <?php endif ?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Отправление:</div>
                                <div class="text-muted"><?= esc(Time::parse($flight['departure'])->toDateString()); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Прибытие:</div>
                                <div class="text-muted"><?= esc(Time::parse($flight['arrival'])->toDateString() ); ?></div>
                            </div>

                            <a class="btn btn-primary" href="<?= base_url()?>/index.php/flight/edit/<?= esc($flight['id']); ?>">Редактировать</a>
                            <a class="btn btn-danger" href="<?= base_url()?>/index.php/flight/delete/<?= esc($flight['id']); ?>">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Рейс не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>