<!DOCTYPE html>
<head>
    <title>Система просмотра грузовых маршрутов</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main{
            margin-top: 90px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <a class="navbar-brand" style="color:white" href="<?= base_url()?>"><svg width="30" height="30" viewBox="0 0 448 448" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <path d="M224.001 0C156.058 0.005 98.75 3.749 90.204 8.742C75.239 13.834 64.02 47.77 64.001 88V108H59.917C59.484 107.06 58.931 106.214 58.28 105.496C60.006 103.601 60.997 100.871 61.001 98V88C61.001 82.477 57.419 78.001 53.001 78H37.001C32.583 78.001 29.001 82.477 29.001 88V98C29.003 100.874 29.994 103.607 31.722 105.504C29.996 107.399 29.005 110.129 29.001 113V163C29.001 168.523 32.583 172.999 37.001 173H53.001C55.855 172.995 58.492 171.09 59.919 168H64.001V258C64.009 261.568 67.82 264.864 74.001 266.648V278.326C65.177 279.465 59.009 283.443 59.001 288V388C59.009 402.275 62.82 415.46 69.001 422.598V439.782C69.001 444.321 72.681 448.001 77.22 448.001H110.782C115.321 448.001 119.001 444.321 119.001 439.782V428H329.001V439.781C329.001 444.32 332.681 448 337.22 448H370.782C375.321 448 379.001 444.32 379.001 439.781V422.584C385.18 415.449 388.991 402.27 389.001 388V288C388.99 283.445 382.822 279.469 374.001 278.33V266.646C380.181 264.862 383.991 261.567 384.001 258V168H388.083C389.51 171.09 392.147 172.995 395.001 173H411.001C415.419 172.999 419.001 168.523 419.001 163V113C418.997 110.129 418.006 107.399 416.28 105.504C418.007 103.607 418.998 100.874 419.001 98V88C419.001 82.477 415.419 78.001 411.001 78H395.001C390.583 78.001 387.001 82.477 387.001 88V98C387.005 100.871 387.996 103.601 389.722 105.496C389.071 106.214 388.519 107.06 388.085 108H384V88C383.987 47.549 372.654 13.488 357.594 8.643C348.443 3.695 291.417 0.006 224.001 0ZM86.844 58H102.666H137.141H310.844H345.334H361.141C365.917 58.001 370.027 61.378 370.952 66.064C372.963 76.254 373.988 87.26 374.001 98.423V98.435V168C374 173.523 369.524 177.999 364.001 178H336.001H112.001H84.001C78.478 177.999 74.002 173.523 74.001 168V98.436V98.428C74.008 87.268 75.027 76.263 77.032 66.071C77.954 61.381 82.065 58 86.844 58ZM99 278H120.428C123.084 277.999 125.277 280.075 125.422 282.727L128.992 347.727C129.149 350.591 126.868 353 124 353H99C96.239 353 94 350.761 94 348V283C94.001 280.239 96.24 278 99 278ZM327.573 278H349C351.761 278 354 280.239 354 283V348C354 350.761 351.761 353 349 353H324C321.131 353 318.851 350.591 319.008 347.727L322.578 282.727C322.724 280.075 324.917 278 327.573 278ZM141.5 290H306.5C307.881 290 309 291.119 309 292.5V295.5C309 296.881 307.881 298 306.5 298H141.5C140.119 298 139 296.881 139 295.5V292.5C139.001 291.12 140.12 290 141.5 290ZM142.5 308H305.5C306.881 308 308 309.119 308 310.5V313.5C308 314.881 306.881 316 305.5 316H142.5C141.119 316 140 314.881 140 313.5V310.5C140.001 309.12 141.12 308 142.5 308ZM143.5 326H304.5C305.881 326 307 327.119 307 328.5V331.5C307 332.881 305.881 334 304.5 334H143.5C142.119 334 141 332.881 141 331.5V328.5C141.001 327.12 142.12 326 143.5 326ZM144.5 344H303.5C304.881 344 306 345.119 306 346.5V349.5C306 350.881 304.881 352 303.5 352H144.5C143.119 352 142 350.881 142 349.5V346.5C142.001 345.12 143.12 344 144.5 344Z" fill="black"/>
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="448.001" height="448.001" fill="white"/>
                </clipPath>
            </defs>
        </svg></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Рейсы
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="<?= base_url()?>/index.php/flight">Все рейсы</a>
                    <a class="dropdown-item" href="<?= base_url()?>/flight/store">Создать рейс</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Мой профиль</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="#">Мой профиль</a>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <?php if (! $ionAuth->loggedIn()): ?>
                <div class="nav-item dropdown">
                    <a class="nav-link active" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Вход</a>
                </div>
            <?php else: ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ><span class="fas fa fa-user-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?= base_url()?>/auth/logout"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Выход</a>
                    </div>
                </li>
            <?php endif ?>
        </ul>
    </div>
</nav>
<main role="main">
    <div class="position-fixed bottom-0 right-0 p-3">
        <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
            <div class="toast-header hide">
                <svg width="30" height="30" viewBox="0 0 448 448" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path d="M224.001 0C156.058 0.005 98.75 3.749 90.204 8.742C75.239 13.834 64.02 47.77 64.001 88V108H59.917C59.484 107.06 58.931 106.214 58.28 105.496C60.006 103.601 60.997 100.871 61.001 98V88C61.001 82.477 57.419 78.001 53.001 78H37.001C32.583 78.001 29.001 82.477 29.001 88V98C29.003 100.874 29.994 103.607 31.722 105.504C29.996 107.399 29.005 110.129 29.001 113V163C29.001 168.523 32.583 172.999 37.001 173H53.001C55.855 172.995 58.492 171.09 59.919 168H64.001V258C64.009 261.568 67.82 264.864 74.001 266.648V278.326C65.177 279.465 59.009 283.443 59.001 288V388C59.009 402.275 62.82 415.46 69.001 422.598V439.782C69.001 444.321 72.681 448.001 77.22 448.001H110.782C115.321 448.001 119.001 444.321 119.001 439.782V428H329.001V439.781C329.001 444.32 332.681 448 337.22 448H370.782C375.321 448 379.001 444.32 379.001 439.781V422.584C385.18 415.449 388.991 402.27 389.001 388V288C388.99 283.445 382.822 279.469 374.001 278.33V266.646C380.181 264.862 383.991 261.567 384.001 258V168H388.083C389.51 171.09 392.147 172.995 395.001 173H411.001C415.419 172.999 419.001 168.523 419.001 163V113C418.997 110.129 418.006 107.399 416.28 105.504C418.007 103.607 418.998 100.874 419.001 98V88C419.001 82.477 415.419 78.001 411.001 78H395.001C390.583 78.001 387.001 82.477 387.001 88V98C387.005 100.871 387.996 103.601 389.722 105.496C389.071 106.214 388.519 107.06 388.085 108H384V88C383.987 47.549 372.654 13.488 357.594 8.643C348.443 3.695 291.417 0.006 224.001 0ZM86.844 58H102.666H137.141H310.844H345.334H361.141C365.917 58.001 370.027 61.378 370.952 66.064C372.963 76.254 373.988 87.26 374.001 98.423V98.435V168C374 173.523 369.524 177.999 364.001 178H336.001H112.001H84.001C78.478 177.999 74.002 173.523 74.001 168V98.436V98.428C74.008 87.268 75.027 76.263 77.032 66.071C77.954 61.381 82.065 58 86.844 58ZM99 278H120.428C123.084 277.999 125.277 280.075 125.422 282.727L128.992 347.727C129.149 350.591 126.868 353 124 353H99C96.239 353 94 350.761 94 348V283C94.001 280.239 96.24 278 99 278ZM327.573 278H349C351.761 278 354 280.239 354 283V348C354 350.761 351.761 353 349 353H324C321.131 353 318.851 350.591 319.008 347.727L322.578 282.727C322.724 280.075 324.917 278 327.573 278ZM141.5 290H306.5C307.881 290 309 291.119 309 292.5V295.5C309 296.881 307.881 298 306.5 298H141.5C140.119 298 139 296.881 139 295.5V292.5C139.001 291.12 140.12 290 141.5 290ZM142.5 308H305.5C306.881 308 308 309.119 308 310.5V313.5C308 314.881 306.881 316 305.5 316H142.5C141.119 316 140 314.881 140 313.5V310.5C140.001 309.12 141.12 308 142.5 308ZM143.5 326H304.5C305.881 326 307 327.119 307 328.5V331.5C307 332.881 305.881 334 304.5 334H143.5C142.119 334 141 332.881 141 331.5V328.5C141.001 327.12 142.12 326 143.5 326ZM144.5 344H303.5C304.881 344 306 345.119 306 346.5V349.5C306 350.881 304.881 352 303.5 352H144.5C143.119 352 142 350.881 142 349.5V346.5C142.001 345.12 143.12 344 144.5 344Z" fill="black"/>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="448.001" height="448.001" fill="white"/>
                        </clipPath>
                    </defs>
                </svg>
                <strong class="mr-auto">TransTycoon</strong>
                <small>Сейчас</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                <?= session()->getFlashdata('message') ?>
            </div>
        </div>
    </div>
    <?php if (session()->getFlashdata('message')) :?>
        <script>
            $('.toast').toast('show');
        </script>
    <?php endif ?>

    <?= $this->renderSection('content') ?>
</main>
<footer class="text-center">
    <p>© Semenov Eugene 2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>/index.php/pages/view/agreement">Пользовательское соглашение</a></p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>