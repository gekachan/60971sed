<?php namespace App\Controllers;

use App\Models\FlightModel;
use App\Models\RouteModel;
use App\Models\TransportModel;
use Aws\S3\S3Client;
use CodeIgniter\Controller;
use Kint\Object\TraceFrameObject;
use phpDocumentor\Reflection\Types\Array_;

class Flight extends BaseController

{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FlightModel();
        $routemodel = new RouteModel();
        $data ['flight'] = $model->getFlight();
        $data ['route'] = $routemodel->getRoute();
        echo view('flight/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FlightModel();
        $routemodel = new RouteModel();
        $transportmodel = new TransportModel();
        $data ['flight'] = $model->getFlight($id);
        $data ['route'] = $routemodel->getRoute();
        $data ['transport'] = $transportmodel->getTransport();
        echo view('flight/view', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_route' => 'required|integer',
                'id_transport'  => 'required|decimal',
                'departure'  => 'required|valid_date',
                'arrival'  => 'required|valid_date',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new FlightModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_route' => $this->request->getPost('id_route'),
                'id_transport' => $this->request->getPost('id_transport'),
                'departure' => $this->request->getPost('departure'),
                'arrival' => $this->request->getPost('arrival'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);

            session()->setFlashdata('message', lang('Transport.f_create_success'));
            return redirect()->to('/flight');
        }
        else
        {
            return redirect()->to('/flight/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $routemodel = new RouteModel();
        $transportmodel = new TransportModel();
        $data ['route'] = $routemodel->getRoute();
        $data ['transport'] = $transportmodel->getTransport();
        echo view('flight/create', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_route' => 'required|integer',
                'id_transport'  => 'required|decimal',
                'departure'  => 'required|valid_date',
                'arrival'  => 'required|valid_date',
            ]))
        {
            $model = new FlightModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'id_route' => $this->request->getPost('id_route'),
                'id_transport' => $this->request->getPost('id_transport'),
                'departure' => $this->request->getPost('departure'),
                'arrival' => $this->request->getPost('arrival'),
            ]);
            session()->setFlashdata('message', lang('Transport.f_update_success'));
            return redirect()->to('/flight');
        }
        else
        {
            return redirect()->to('/flight/edit/'.$this->request->getPost('id'))->withInput();
        }


    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FlightModel();

        helper(['form']);
        $data ['flight'] = $model->getFlight($id);
        $routemodel = new RouteModel();
        $transportmodel = new TransportModel();
        $data ['route'] = $routemodel->getRoute();
        $data ['transport'] = $transportmodel->getTransport();
        $data ['validation'] = \Config\Services::validation();
        echo view('flight/edit', $this->withIon($data));

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FlightModel();
        $model->delete($id);
        return redirect()->to('/flight');
    }
}