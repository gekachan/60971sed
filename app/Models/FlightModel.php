<?php namespace App\Models;
use CodeIgniter\Model;
class FlightModel extends Model
{
    protected $table = 'flight'; //таблица, связанная с моделью
    protected $allowedFields = ['id_route', 'id_transport', 'departure', 'arrival', 'picture_url'];
    public function getFlight($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}