<?php namespace App\Models;
use CodeIgniter\Model;
class RouteModel extends Model
{
    protected $table = 'route'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'rate', 'distance'];
    public function getRoute($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}